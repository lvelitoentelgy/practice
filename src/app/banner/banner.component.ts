import { Component, OnInit } from '@angular/core';
import { Banner } from './banner.class';
import { FirebaseService } from '../firebase.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss','../../../node_modules/bootstrap/dist/css/bootstrap.min.css']
})
export class BannerComponent implements OnInit {

  banners: Banner[];

  constructor(private fireService: FirebaseService, private firestore: AngularFirestore) { 
    this.fireService.getEvents().subscribe(actionArray => {
      this.banners = actionArray.map(item => {
        return {
          id: item.payload.doc.id,
          ...item.payload.doc.data()
        } as Banner;
      })
    });
  }

  ngOnInit() {
  }

}
