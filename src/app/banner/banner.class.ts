export class Banner {
    area: number;
    id: string;
    image: string;
    formulario: string;
    link: string;
    fechaCierre: string;
    dia: string;
    mes: string;
    hora: string;
    copa: string;
    organizado: string;
    lugar: string;
    distrito: string;
    rank: string;
    descripcion: string;
    primero: string;
    segundo: string;
    tercero: string;
    sorteos: string;
}