import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BannersService {

  baseUrl = 'http://localhost:3000';

  constructor(private httpClient: HttpClient) {
  }

  getBanners() {
    return this.httpClient.get(this.baseUrl + '/banners');
  }

}
