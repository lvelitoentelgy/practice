import { Component } from '@angular/core';
import { ItemComponent } from './item/item.component';
import { BannerComponent } from './banner/banner.component';
import { InfoComponent } from './info/info.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss','../../node_modules/bootstrap/dist/css/bootstrap.min.css']
})
export class AppComponent {
  title = 'pvperu';
}
