import { Component, OnInit, ElementRef, ViewChild, } from '@angular/core';
import { Info } from './info.class';
import { timeout } from 'rxjs/operators';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  infos: Info[];
  @ViewChild('infolayer') infolayer: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  _getInfo(event) {
    let data = event;
    this.infos = data;
    console.log(this);
    console.log(this.infolayer);
    let edit = this.infolayer.nativeElement;
    edit.classList.add('blocked');
    setTimeout (() => {
      edit.classList.add('activated');
    }, 100);
  }

  _closeSection(event) {
    let data = event;
    let edit = this.infolayer.nativeElement;
    edit.classList.remove('activated');
    setTimeout (() => {
      edit.classList.remove('blocked');
    }, 600);
  };

}
