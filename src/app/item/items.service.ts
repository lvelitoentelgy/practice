import { Injectable } from '@angular/core';
import { Item } from './items.class';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ItemsService {

  baseUrl = 'http://localhost:3000';

  constructor(private httpClient: HttpClient) {
  }

  getItems() {
    return this.httpClient.get(this.baseUrl + '/events');
  }

}
