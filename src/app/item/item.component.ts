import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Item } from './items.class';
import { FirebaseService } from '../firebase.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss', '../../../node_modules/bootstrap/dist/css/bootstrap.min.css']
})
export class ItemComponent implements OnInit {

  sectionTitle = 'EVENTOS REGISTRADOS';
  sectionDescription = 'Revisa los eventos que tenemos registrados, en ellos puedes encontrar el link al formulario y el link a la página de registro de The Silph Arena';
  items: Item[];
  @Output() sendData = new EventEmitter();

  constructor(private fireService: FirebaseService, private firestore: AngularFirestore) {
    // this.fireService.getEvents().subscribe((data)=>{
    //   console.log(data);
    //   this.items = data;
    // });

    this.fireService.getEvents().subscribe(actionArray => {
      this.items = actionArray.map(item => {
        return {
          id: item.payload.doc.id,
          ...item.payload.doc.data()
        } as Item;
      })
    });
  }

  ngOnInit() {
  }

  openElement(event) {
    let targeted = event.target;
    let parent = targeted.parentNode;
    let elementToShow = parent.querySelectorAll('.calendar-hide')[0];
    let classes = elementToShow.classList[1];
    let toHide = document.querySelectorAll('.calendar-hide');
    for (let i = 0; i < toHide.length; i++) {
      toHide[i].classList.remove('open');
    }
    if (classes !== 'open') {
      elementToShow.classList.add('open');
    } else {
      elementToShow.classList.remove('open');
    }

  }

  _findIndex(event) {
    let targeted = event.target;
    let identifier = targeted.getAttribute('rel');
    let collection = this.items;
    for(let i = 0; i < collection.length; i++) {
      let ides = collection[i].id;
      if(ides == identifier) {
        console.log(collection[i]);
        let element = [];
        element.push(collection[i]);
        this.sendData.emit(element);
      } else {
        console.log('theres no number like that');
      }
    }
  }

}
