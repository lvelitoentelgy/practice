// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC5NDxufH0_ZkXX-Mqe4bYQaKsvwfcdwN4",
    authDomain: "pvperu-ad505.firebaseapp.com",
    databaseURL: "https://pvperu-ad505.firebaseio.com",
    projectId: "pvperu-ad505",
    storageBucket: "",
    messagingSenderId: "882987719335",
    appId: "1:882987719335:web:067120deb733d839"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
